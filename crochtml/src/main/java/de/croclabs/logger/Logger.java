package de.croclabs.logger;

public final class Logger<T> {
    private Class<T> clazz = null;

    public Logger(Class<T> clazz) {
        super();
        this.clazz = clazz;
    }

    public void error(String message) {
        System.out.println("ERROR " + clazz.getName() + ": " + message);
    }

    public void debug(String message) {
        System.out.println("DEBUG " + clazz.getName() + ": " + message);
    }

    public void info(String message) {
        System.out.println("INFO " + clazz.getName() + ": " + message);
    }
}
