package de.croclabs.logger;

public final class LoggerFactory {
    private LoggerFactory() {
        super();
    }

    public static <T> Logger<T> getLog(Class<T> clazz) {
        return new Logger<>(clazz);
    }
}
