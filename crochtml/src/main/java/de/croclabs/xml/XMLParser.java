package de.croclabs.xml;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public final class XMLParser {
    private static XMLReader reader = null;

    private XMLParser() {
        super();
    }

    public static XMLReader getInstance() {
        if (reader == null) {
            try {
                reader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            } catch (SAXException | ParserConfigurationException e) {
                e.printStackTrace();
            }
        }

        return reader;
    }

}
